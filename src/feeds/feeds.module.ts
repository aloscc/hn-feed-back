import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FeedsController } from './feeds.controller';
import { FeedsService } from './feeds.service';
import { Feed, FeedSchema } from './schemas/feed.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Feed.name, schema: FeedSchema }]),
    HttpModule,
  ],
  controllers: [FeedsController],
  providers: [FeedsService],
})
export class FeedsModule {}
