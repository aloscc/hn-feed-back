import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';

export type FeedDocument = Feed & Document;

@Schema()
export class Feed {
  @Prop(SchemaTypes.ObjectId)
  _id: number;

  @Prop()
  story_title: string;

  @Prop()
  title: string;

  @Prop()
  story_url: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  created_at: Date;

  @Prop()
  status: number;
}

export const FeedSchema = SchemaFactory.createForClass(Feed);
