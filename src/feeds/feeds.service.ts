import { Injectable, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Feed, FeedDocument } from './schemas/feed.schema';
import { CreateFeedDto } from './dto/create-feed.dto';
import { ReadFeedDto } from './dto/read-feed.dto';
import { timer } from 'rxjs';
import { plainToClass } from 'class-transformer';
import { Status } from '../shared/enums/status.enum';
import { Constants } from '../shared/constants';

@Injectable()
export class FeedsService {
  constructor(
    @InjectModel(Feed.name) private feedModel: Model<FeedDocument>,
    private httpService: HttpService,
  ) {
    this.triggerToApi();
  }

  async createFeed(createFeedDto: CreateFeedDto): Promise<Feed> {
    const createdFeed = new this.feedModel(createFeedDto);
    createdFeed._id = parseInt(createFeedDto.objectID);
    createdFeed.status = Status.ACTIVE;
    return createdFeed.save();
  }

  async getFeeds(): Promise<Feed[]> {
    return this.feedModel.find({ status: Status.ACTIVE }).exec();
  }

  async findById(id: number): Promise<Feed> {
    const feed = await this.feedModel.findById(id).exec();
    return feed;
  }

  async clearFeeds(): Promise<void> {
    await this.feedModel.deleteMany({});
  }

  async deleteFeed(id: number): Promise<void> {
    await this.feedModel.findByIdAndUpdate(
      id,
      { status: Status.INACTIVE },
      { new: true },
    );
  }

  async updateFeeds(createFeedDtos: Array<CreateFeedDto>): Promise<void> {
    createFeedDtos.forEach(
      async (createFeedDto: CreateFeedDto) =>
        await this.createFeed(createFeedDto),
    );
  }

  async pullFeedsFromApi(): Promise<Array<CreateFeedDto>> {
    const { data } = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    const feeds: Feed[] = data.hits;
    return plainToClass(CreateFeedDto, feeds);
  }

  async triggerToApi(): Promise<void> {
    await this.clearFeeds();
    await this.updateFeeds(await this.pullFeedsFromApi());
    timer(0, Constants.TIME_INTERVAL).subscribe(async () => {
      await this.clearFeeds();
      await this.updateFeeds(await this.pullFeedsFromApi());
    });
  }
}
