import { Test, TestingModule } from '@nestjs/testing';
import { FeedsService } from './feeds.service';
import { Model } from 'mongoose';
import { Feed, FeedDocument } from './schemas/feed.schema';
import { getModelToken } from '@nestjs/mongoose';
import { HttpService } from '@nestjs/common';
import { Status } from '../shared/enums/status.enum';

const mockFeedModel = () => ({
  find: jest.fn().mockImplementationOnce(() => ({ exec: jest.fn() })),
  deleteMany: jest.fn(),
  findById: jest.fn(),
  findByIdAndUpdate: jest.fn(),
});

const mockHttpService = () => ({
  get: jest.fn(),
});

describe('FeedsService', () => {
  let feedService: FeedsService;
  let httpService;
  let feedModel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FeedsService,
        { provide: getModelToken(Feed.name), useFactory: mockFeedModel },
        { provide: HttpService, useFactory: mockHttpService },
      ],
    }).compile();

    feedService = await module.get<FeedsService>(FeedsService);
    httpService = await module.get<HttpService>(HttpService);
    feedModel = await module.get<Model<FeedDocument>>(getModelToken(Feed.name));
  });

  it('should be defined', () => {
    expect(feedService).toBeDefined();
  });
  describe('getFeeds', () => {
    it('Get all feeds', async () => {
      expect(feedModel.find).not.toHaveBeenCalled();
      const feeds = await feedService.getFeeds();
      expect(feedModel.find).toHaveBeenCalled();
    });
  });

  describe('deleteFeed', () => {
    it('Delete a feed', async () => {
      expect(feedModel.findByIdAndUpdate).not.toHaveBeenCalled();
      const feedId = 1;
      await feedService.deleteFeed(feedId);
      expect(feedModel.findByIdAndUpdate).toBeCalledWith(
        feedId,
        { status: Status.INACTIVE },
        { new: true },
      );
    });
  });
});
