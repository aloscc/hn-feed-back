import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class ReadFeedDto {
  @Expose()
  readonly _id: number;
  @Expose()
  readonly story_title: string;
  @Expose()
  readonly title: string;
  @Expose()
  readonly story_url: string;
  @Expose()
  readonly url: string;
  @Expose()
  readonly author: string;
  @Expose()
  readonly created_at: Date;
}
