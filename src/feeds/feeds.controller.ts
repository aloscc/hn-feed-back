import { Controller, Get, Delete, ParseIntPipe, Param } from '@nestjs/common';
import { FeedsService } from './feeds.service';
import { ApiResponse } from 'src/shared/interfaces/api-response.interface';

@Controller('feeds')
export class FeedsController {
  constructor(private readonly _feedsService: FeedsService) {}
  @Get()
  async findAll(): Promise<ApiResponse> {
    return {
      message: 'Feeds List',
      data: await this._feedsService.getFeeds(),
    };
  }

  @Get('/:feedId')
  async findOne(
    @Param('feedId', ParseIntPipe) feedId: number,
  ): Promise<ApiResponse> {
    return {
      message: 'Feeds found',
      data: await this._feedsService.findById(feedId),
    };
  }

  @Delete('/:feedId')
  async deleteFeed(
    @Param('feedId', ParseIntPipe) feedId: number,
  ): Promise<ApiResponse> {
    await this._feedsService.deleteFeed(feedId);
    return {
      message: 'The Feedd was deleted successfully',
      data: '',
    };
  }
}
