import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FeedsModule } from './feeds/feeds.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    FeedsModule,
    MongooseModule.forRoot(
      'mongodb://mongo:27017/feed?readPreference=primary&appname=MongoDB%20Compass&ssl=false',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
